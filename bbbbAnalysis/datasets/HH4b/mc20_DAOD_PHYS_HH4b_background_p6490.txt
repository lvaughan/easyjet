# dijet b-filtered
mc20_13TeV.802067.Py8EG_A14NNPDF23LO_jetjet_JZ1_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802067.Py8EG_A14NNPDF23LO_jetjet_JZ1_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802067.Py8EG_A14NNPDF23LO_jetjet_JZ1_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.800285.Py8EG_A14NNPDF23LO_jetjet_JZ2_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.800285.Py8EG_A14NNPDF23LO_jetjet_JZ2_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.800285.Py8EG_A14NNPDF23LO_jetjet_JZ2_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.800286.Py8EG_A14NNPDF23LO_jetjet_JZ3_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.800286.Py8EG_A14NNPDF23LO_jetjet_JZ3_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.800286.Py8EG_A14NNPDF23LO_jetjet_JZ3_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.800287.Py8EG_A14NNPDF23LO_jetjet_JZ4_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.800287.Py8EG_A14NNPDF23LO_jetjet_JZ4_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.800287.Py8EG_A14NNPDF23LO_jetjet_JZ4_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.802068.Py8EG_A14NNPDF23LO_jetjet_JZ5_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802068.Py8EG_A14NNPDF23LO_jetjet_JZ5_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802068.Py8EG_A14NNPDF23LO_jetjet_JZ5_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.802069.Py8EG_A14NNPDF23LO_jetjet_JZ6_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802069.Py8EG_A14NNPDF23LO_jetjet_JZ6_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802069.Py8EG_A14NNPDF23LO_jetjet_JZ6_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.802070.Py8EG_A14NNPDF23LO_jetjet_JZ7_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802070.Py8EG_A14NNPDF23LO_jetjet_JZ7_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802070.Py8EG_A14NNPDF23LO_jetjet_JZ7_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.802071.Py8EG_A14NNPDF23LO_jetjet_JZ8_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802071.Py8EG_A14NNPDF23LO_jetjet_JZ8_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802071.Py8EG_A14NNPDF23LO_jetjet_JZ8_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490
mc20_13TeV.802072.Py8EG_A14NNPDF23LO_jetjet_JZ9incl_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13144_p6490
mc20_13TeV.802072.Py8EG_A14NNPDF23LO_jetjet_JZ9incl_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13145_p6490
mc20_13TeV.802072.Py8EG_A14NNPDF23LO_jetjet_JZ9incl_4jets15_2bjets.deriv.DAOD_PHYS.e8547_s3797_r13167_p6490

# ttbar
mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_PHYS.e6337_s3681_r13144_p6490
mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_PHYS.e6337_s3681_r13145_p6490
mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_PHYS.e6337_s3681_r13167_p6490
